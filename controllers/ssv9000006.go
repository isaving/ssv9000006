//Version: v1.0.0.0
package controllers

import (
	constants "git.forms.io/isaving/sv/ssv9000006/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000006/services"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000006Controller struct {
    controllers.CommTCCController
}

func (*Ssv9000006Controller) ControllerName() string {
	return "Ssv9000006Controller"
}

// @Desc ssv9000006 controller
// @Author
// @Date 2020-12-20
func (c *Ssv9000006Controller) Ssv9000006() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000006Controller.Ssv9000006 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000006I := &models.SSV9000006I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv9000006I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  	if err := ssv9000006I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000006 := &services.Ssv9000006Impl{} 
	ssv9000006.New(c.CommTCCController)
  	ssv9000006.Ssv9000006I = ssv9000006I
	ssv9000006Compensable := services.Ssv9000006Compensable

	proxy, err := aspect.NewDTSProxy(ssv9000006, ssv9000006Compensable, c.DTSCtx)
	if err != nil {
		log.Errorf("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv9000006I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv9000006O, ok := rsp.(*models.SSV9000006O); ok {
		if responseBody, err := models.PackResponse(ssv9000006O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
} 
// @Title Ssv9000006 Controller
// @Description ssv9000006 controller
// @Param Ssv9000006 body models.SSV9000006I true body for SSV9000006 content
// @Success 200 {object} models.SSV9000006O
// @router /create [post]
func (c *Ssv9000006Controller) SWSsv9000006() {
	//Here is to generate API documentation, no need to implement methods
}
