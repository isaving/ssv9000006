package dao

import (
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type TMdslagp struct {
	MediaAgreementId	string		//介质和合约关系ID
	MdsType       		string      //介质类型
	MdsNm     			string      //介质号码
	Currency       	 	string      //币别
	CashtranFlag		string		//钞汇标志
	ChannelNm       	string      //渠道号
	UsgCod     			string      //用途代码
	AgreementId			string      //合约号
	AgreementType		string      //合约类型
	DefaultAgreement	string      //默认合约标识
	EffectiveFlag		string      //有效标志
	LastUpDatetime		string      //最后更新日期时间
	LastUpBn     		string      //最后更新行所
	LastUpEm        	string      //最后更新柜员
	TccState			string
}

func InsertTMdslagp(TMdslagp *TMdslagp, o *xorm.Engine) error {
	TMdslagp.TccState = "1"
	if _, err := EngineCache.Omit().Insert(TMdslagp); nil != err {
		log.Errorf("Insert TMdslagp Faild, err: %s",err)
		return  err
	}
	return nil
}