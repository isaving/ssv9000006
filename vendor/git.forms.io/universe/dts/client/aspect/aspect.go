//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package aspect

import (
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/callback"
	"git.forms.io/universe/dts/client/repository"
	"git.forms.io/universe/dts/client/transaction"
	"git.forms.io/universe/dts/common/comm"
	constant "git.forms.io/universe/dts/common/const"
	"git.forms.io/universe/dts/common/log"
	"git.forms.io/universe/dts/common/util"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/config"
	"github.com/go-errors/errors"
	"reflect"
	"strings"
	"time"
)

// @Desc DTS Proxy information
type DTSProxy struct {
	TxnInvocation client.TransactionInvocation
	CmpTxnRep     repository.CompensableTxnRepository
	DTSCtx        *compensable.TxnCtx
	TxnManager    transaction.TxnManager
}

var dtsProxyStorage = make(map[string]*client.TransactionInvocation)

// @Desc services use this method to new a DTS Proxy
// @Param txnInstance a instance of the transcation
// @Param compensable a struct includes TCC's method name and Timeout time
// @Param dtsCtx some information of DTS Agent
// @Return ret a DTSProxy
// @Return err error
func NewDTSProxy(txnInstance interface{}, compensable client.Compensable, dtsCtx *compensable.TxnCtx) (ret *DTSProxy, err error) {
	dtsProxy := &DTSProxy{}
	tv := reflect.ValueOf(txnInstance)

	isExistConfirmMethod := "" != compensable.ConfirmMethod && "" != strings.TrimSpace(compensable.ConfirmMethod)
	isExistCancelMethod := "" != compensable.CancelMethod && "" != strings.TrimSpace(compensable.CancelMethod)

	// 1. check try、confirm、cancel method whether exists
	txnInsType := reflect.TypeOf(txnInstance)

	if _, ok := txnInsType.MethodByName(compensable.TryMethod); !ok {
		err = errors.Errorf("Cannot found try method:[%s]", compensable.TryMethod)
		return
	}

	if isExistConfirmMethod {
		if _, ok := txnInsType.MethodByName(compensable.ConfirmMethod); !ok {
			err = errors.Errorf("Cannot found confirm method:[%s]", compensable.ConfirmMethod)
			return
		}
	}

	if isExistCancelMethod {
		if _, ok := txnInsType.MethodByName(compensable.CancelMethod); !ok {
			err = errors.Errorf("Cannot found cancel method:[%s]", compensable.CancelMethod)
			return
		}
	}
	// 2. check try、confirm method whether has the same parameters
	tryMethod := tv.MethodByName(compensable.TryMethod)

	if isExistConfirmMethod {
		confirmMethod := tv.MethodByName(compensable.ConfirmMethod)
		if tryMethod.Type().NumIn() != confirmMethod.Type().NumIn() {
			err = errors.Errorf("try and confirm method must has the same parameters!")
			return
		}
	}

	// 3. check try、cancel method whether has the same parameters
	if isExistCancelMethod {
		cancelMethod := tv.MethodByName(compensable.CancelMethod)
		if tryMethod.Type().NumIn() != cancelMethod.Type().NumIn() {
			err = errors.Errorf("try and cancel method must has the same parameters!")
			return
		}
	}

	//4. save txn's invocation information to dtsProxy
	dtsProxy.TxnInvocation.ReflectValueOfTxnInstance = tv
	dtsProxy.TxnInvocation.TxnInstance = txnInstance
	dtsProxy.CmpTxnRep = repository.CmpTxnRep

	dtsProxy.TxnInvocation.CompensableTxn.Timeout = compensable.Timeout
	dtsProxy.TxnInvocation.CompensableTxn.TryMethod = compensable.TryMethod
	dtsProxy.TxnInvocation.CompensableTxn.ConfirmMethod = compensable.ConfirmMethod
	dtsProxy.TxnInvocation.CompensableTxn.CancelMethod = compensable.CancelMethod
	dtsProxy.TxnInvocation.NumOfParam = tryMethod.Type().NumIn()

	dtsProxy.DTSCtx = dtsCtx
	log.Debugf("Register DTS service[%v]", compensable)

	ret = dtsProxy
	return
}

func genCompensableFlag(compensableTxn client.Compensable) int {
	compensableFlagSet := 0

	if "" != compensableTxn.ConfirmMethod && "" != strings.TrimSpace(compensableTxn.ConfirmMethod) {
		compensableFlagSet = compensableFlagSet | transaction.CONFIRM_FLAG
	}

	if "" != compensableTxn.CancelMethod && "" != strings.TrimSpace(compensableTxn.CancelMethod) {
		compensableFlagSet = compensableFlagSet | transaction.CANCEL_FLAG
	}

	return compensableFlagSet
}

func rootRegister(txnManager transaction.TxnManager, spanCtx *compensable.SpanContext, compensableFlagSet int, currentCmpTxnCtx *compensable.TxnCtx) (dtsAgentAddress string, err error) {
	//txnManager := transaction.NewTxnManager(comm.NewRemoteClientFactory().CreateClient())
	cfg := config.CmpSvrConfig
	if strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType) {
		// DTS agent address + "|" + Register Path + "|" + Enlist Path + "|" + Result try report Path
		dtsAgentAddress = cfg.DtsAgentAddress + "|" +
			cfg.RegisterUrlPath + "|" +
			cfg.EnlistUrlPath + "|" +
			cfg.TryResultReportUrlPath
	} else {
		// ORG + "|" + DCN + "|" + NODE ID + "|" + Instance ID + "|" + Register TopicID + "|" + Enlist TopicID + "|" + Result try report TopicID
		dtsAgentAddress = cfg.DtsAgentOrganization + "|" +
			cfg.DtsAgentDataCenterNode + "|" +
			cfg.DtsAgentNodeId + "|" +
			cfg.DtsAgentInstanceId + "|" +
			cfg.DtsAgentRegisterTopicName + "|" +
			cfg.DtsAgentEnlistTopicName + "|" +
			cfg.DtsAgentTryResultReportName
	}
	// register ROOT transaction
	if rootXid, err := txnManager.Register(spanCtx, dtsAgentAddress, compensableFlagSet); nil != err {
		log.Errorf("Register root transaction failed,error: %v", err)
		return "", err
	} else {
		currentCmpTxnCtx.RootXid = rootXid
		currentCmpTxnCtx.ParentXid = rootXid
		currentCmpTxnCtx.BranchXid = rootXid
		currentCmpTxnCtx.Level = 0
		currentCmpTxnCtx.CurrentTime = util.CurrentTime()
		currentCmpTxnCtx.LastUpdateTime = currentCmpTxnCtx.CurrentTime
		currentCmpTxnCtx.DtsAgentAddress = dtsAgentAddress
	}

	return dtsAgentAddress, nil
}

func branchEnlist(txnManager transaction.TxnManager, spanCtx *compensable.SpanContext, cmpTxnCtx *compensable.TxnCtx, compensableFlagSet int,
	currentCmpTxnCtx *compensable.TxnCtx) (dtsAgentAddress string, err error) {

	//txnManager := transaction.NewTxnManager(comm.NewRemoteClientFactory().CreateClient())

	dtsAgentAddress = cmpTxnCtx.DtsAgentAddress
	// enlist BRANCH transaction
	currentCmpTxnCtx.RootXid = cmpTxnCtx.RootXid
	currentCmpTxnCtx.ParentXid = cmpTxnCtx.ParentXid
	currentCmpTxnCtx.DtsAgentAddress = cmpTxnCtx.DtsAgentAddress
	if branchXid, err := txnManager.Enlist(spanCtx, dtsAgentAddress, currentCmpTxnCtx.RootXid, currentCmpTxnCtx.ParentXid, compensableFlagSet); err != nil {
		log.Errorf("Enlist branch transaction failed, error: %v", err)
		return "", err
	} else {
		currentCmpTxnCtx.Level++
		currentCmpTxnCtx.BranchXid = branchXid
	}

	return dtsAgentAddress, nil
}

func changeTxnInvocationState(compensableFlagSet int, currentCmpTxnCtx *compensable.TxnCtx, targetMethodResult []reflect.Value, p *DTSProxy) bool {

	isOk := nil == targetMethodResult[len(targetMethodResult)-1].Interface()
	////change TxnInvocation State
	if isOk {
		p.TxnInvocation.SetState(constant.TRIED_OK)
		// if cancel suspend, then cancel the transaction immediately
		if p.TxnInvocation.IsCancelSuspend() {
			if transaction.IsFlag(compensableFlagSet, transaction.CANCEL_FLAG) {
				callback.NewTxnCallback().Cancel(currentCmpTxnCtx.RootXid, currentCmpTxnCtx.BranchXid)
			} else {
				log.Debugf("No cancel method, Skip cancel suspend,Root Xid[%s], Branch Xid[%s]", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid)
				p.CmpTxnRep.DeleteTxnByXid(currentCmpTxnCtx.RootXid, currentCmpTxnCtx.BranchXid)
			}
		}
	} else {
		p.TxnInvocation.SetState(constant.TRIED_FAILED)
	}

	return isOk
}

func doTryResultReport(isOk bool, txnManager transaction.TxnManager, dtsAgentAddress string, spanCtx *compensable.SpanContext,
	compensableFlagSet int, currentCmpTxnCtx *compensable.TxnCtx, p *DTSProxy) error {
	if err := txnManager.ReportTryResult(spanCtx, dtsAgentAddress, currentCmpTxnCtx.RootXid, currentCmpTxnCtx.ParentXid, currentCmpTxnCtx.BranchXid, isOk); err != nil {
		log.Errorf("Report try result failed,error: %v", err)
		return err
	}

	log.Debugf("Report transaction try result successfully!")
	// R/R mode: if current transaction is ROOT，then execute the confirm/cancel method with themselves，DTS Agent won't callback anymore.
	txnCallback := callback.NewTxnCallback()
	if isOk {
		if !transaction.IsFlag(compensableFlagSet, transaction.CONFIRM_FLAG) {
			log.Debugf("No confirm method, Skip confirm,Root Xid[%s], Branch Xid[%s]", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid)
			p.CmpTxnRep.DeleteTxnByXid(currentCmpTxnCtx.RootXid, currentCmpTxnCtx.BranchXid)
			return nil
		}
		if _, err := txnCallback.Confirm(currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid); err != nil {
			log.Errorf("Root transaction confirm failed, Root Xid[%s], Branch Xid[%s], error: %v", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid, err)
			return err
		} else {
			log.Debugf("Root transaction confirm successfully, Root Xid[%s], Branch Xid[%s]", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid)
		}
	} else {
		if !transaction.IsFlag(compensableFlagSet, transaction.CANCEL_FLAG) {
			log.Debugf("No cancel method, Skip cancel,Root Xid[%s], Branch Xid[%s]", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid)
			p.CmpTxnRep.DeleteTxnByXid(currentCmpTxnCtx.RootXid, currentCmpTxnCtx.BranchXid)
			return nil
		}

		if _, err := txnCallback.Cancel(currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid); err != nil {
			log.Errorf("Root transaction cancel failed, Root Xid[%s], Branch Xid[%s], error: %v", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid, err)
			return err
		} else {
			log.Debugf("Root transaction cancel successfully, Root Xid[%s], Branch Xid[%s]", currentCmpTxnCtx.RootXid, currentCmpTxnCtx.RootXid)
		}
	}

	return nil
}

//isRoot Judge the txn is root txn or not
func isRoot(cmpTxnCtx *compensable.TxnCtx) bool {
	return (nil == cmpTxnCtx) || (cmpTxnCtx.RootXid == "" && cmpTxnCtx.BranchXid == "" && cmpTxnCtx.DtsAgentAddress == "")
}

func decodeInputParams(inputParams []interface{}) []reflect.Value {
	params := make([]reflect.Value, 0)
	for _, param := range inputParams {
		params = append(params, reflect.ValueOf(param))
	}
	return params
}

// @Desc services use this method to excute service with TCC flow
// @Param InputParams the params sent by service
// @Return targetMethodResult the result of TCC flow
func (p *DTSProxy) Do(inputParams ...interface{}) (targetMethodResult []reflect.Value) {
	var dtsAgentAddress string
	var err error
	var txnManager transaction.TxnManager
	var startTime int64

	if p.TxnInvocation.CompensableTxn.Timeout > 0 {
		startTime = time.Now().UnixNano() / 1e6
	}
	log.Debugf("Start do DTS proxy, DTS context:[%++v]", p.DTSCtx)
	params := decodeInputParams(inputParams)

	if config.CmpSvrConfig.DtsPropagator {
		log.Debug("DTS propagator, only invoke try method and propagate DTS context.")
		cv := p.TxnInvocation.ReflectValueOfTxnInstance.Elem()
		cmpTxnCtx := p.DTSCtx
		// set parent XID as current transaction XID(branch xid)
		cmpTxnCtx.BranchXid = cmpTxnCtx.ParentXid
		cv.FieldByName(constant.DTS_CTX_FIELD_NAME).Set(reflect.ValueOf(cmpTxnCtx))
		targetMethodResult = invokeTryMethod(p, params)

		if p.TxnInvocation.CompensableTxn.Timeout > 0 {
			timeCost := time.Now().UnixNano()/1e6 - startTime
			if timeCost > p.TxnInvocation.CompensableTxn.Timeout*1000 {
				e := errors.Errorf("Service execute timeout, service timeout[%d]ms, total time cost[%d]ms!", p.TxnInvocation.CompensableTxn.Timeout*1000, timeCost)
				log.Error(e)
				targetMethodResult[len(targetMethodResult)-1] = reflect.ValueOf(e)
			}
		}

		return targetMethodResult
	}

	compensableFlagSet := genCompensableFlag(p.TxnInvocation.CompensableTxn)

	cv := p.TxnInvocation.ReflectValueOfTxnInstance.Elem()
	if nil == p.TxnManager {
		txnManager = transaction.NewTxnManager(comm.NewRemoteClientFactory().CreateClient())
	} else {
		txnManager = p.TxnManager
	}
	cmpTxnCtx := p.DTSCtx
	currentCmpTxnCtx := compensable.CmpTxnCtsInstance.NewTxnCtx()

	var isOk bool

	var spanCtx *compensable.SpanContext

	if nil != cmpTxnCtx {
		spanCtx = cmpTxnCtx.SpanCtx
	}

	isRoot := isRoot(cmpTxnCtx)

	if isRoot {
		if dtsAgentAddress, err = rootRegister(txnManager, spanCtx, compensableFlagSet, currentCmpTxnCtx); nil != err {
			return []reflect.Value{reflect.ValueOf(err)}
		}
	} else {
		if dtsAgentAddress, err = branchEnlist(txnManager, spanCtx, cmpTxnCtx, compensableFlagSet, currentCmpTxnCtx); nil != err {
			return []reflect.Value{reflect.ValueOf(err)}
		}
	}
	// initialize current transaction state with 'TRYING'
	p.TxnInvocation.SetState(constant.TRYING)

	cv.FieldByName(constant.DTS_CTX_FIELD_NAME).Set(reflect.ValueOf(currentCmpTxnCtx))

	// persistent participant transaction
	p.TxnInvocation.RootXid = currentCmpTxnCtx.RootXid
	p.TxnInvocation.BranchXid = currentCmpTxnCtx.BranchXid
	p.TxnInvocation.StoreParams = params
	//Save TxnInvocation into Memory
	p.CmpTxnRep.Save(&p.TxnInvocation)

	// call the target method
	//instance := reflect.ValueOf(p.TxnInvocation.TxnInstance)
	//targetMethod := instance.MethodByName(p.TxnInvocation.CompensableTxn.TryMethod)
	//targetMethodResult = targetMethod.Call(params)
	targetMethodResult = invokeTryMethod(p, params)

	isOk = changeTxnInvocationState(compensableFlagSet, currentCmpTxnCtx, targetMethodResult, p)

	//check txn execution time
	if isOk && p.TxnInvocation.CompensableTxn.Timeout > 0 {
		timeCost := time.Now().UnixNano()/1e6 - startTime
		if timeCost > p.TxnInvocation.CompensableTxn.Timeout*1000 {
			e := errors.Errorf("Service execute timeout, service timeout[%d]ms, total time cost[%d]ms!", p.TxnInvocation.CompensableTxn.Timeout*1000, timeCost)
			log.Error(e)
			targetMethodResult[len(targetMethodResult)-1] = reflect.ValueOf(e)
			isOk = false
		}
	}

	//when non-root txn recovery the Txn-Context
	if !isRoot {
		cv.FieldByName(constant.DTS_CTX_FIELD_NAME).Set(reflect.ValueOf(cmpTxnCtx))
		return targetMethodResult
	}

	// R/R mode, only ROOT transaction need report try execute result!
	// sync report try execute result
	if err := doTryResultReport(isOk, txnManager, dtsAgentAddress, spanCtx, compensableFlagSet, currentCmpTxnCtx, p); nil != err {
		return []reflect.Value{reflect.ValueOf(err)}
	}
	// ROOT transaction remove the Txn-Context
	cv.FieldByName(constant.DTS_CTX_FIELD_NAME).Set(reflect.ValueOf(&compensable.TxnCtx{}))

	return targetMethodResult
}

func invokeTryMethod(p *DTSProxy, params []reflect.Value) (targetMethodResult []reflect.Value) {
	instance := reflect.ValueOf(p.TxnInvocation.TxnInstance)
	targetMethod := instance.MethodByName(p.TxnInvocation.CompensableTxn.TryMethod)
	targetMethodResult = targetMethod.Call(params)

	return
}
