//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package common

import (
	//"errors"
	"fmt"
	"github.com/go-errors/errors"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/astaxie/beego"
	"github.com/sirupsen/logrus"
	"github.com/snowzach/rotatefilehook"
)

const (
	// Define the time format for printing
	// the log timestamp in the format: YYYY-MM-DD HH24:MI:SS
	timeFormat = "2006-01-02 15:04:05"
)

// Fields type, used to pass to `WithFields`.
var logFields = logrus.Fields{}

// logLevelMap defines all log level types
var logLevelMap = map[string]logrus.Level{
	"debug":   logrus.DebugLevel,
	"info":    logrus.InfoLevel,
	"warning": logrus.WarnLevel,
	"error":   logrus.ErrorLevel,
	"fatal":   logrus.FatalLevel,
	"panic":   logrus.PanicLevel,
}

// setLogLevel provides the caller with a log level
func setLogLevel(level string) {
	logLevel, ok := logLevelMap[level]
	if ok {
		logrus.SetLevel(logLevel)
	}
}

//setLogRotateHook is a internal function,it provides the caller with a log rollback policy
func setLogRotateHook(logFile string) {
	maxSize, err := beego.AppConfig.Int("log:maxSize")
	if err != nil {
		maxSize = 200
	}

	maxAge, err := beego.AppConfig.Int("log:maxDays")
	if err != nil {
		maxAge = 7
	}

	logLevelConf := beego.AppConfig.String("log::logLevel")
	logLevel, ok := logLevelMap[logLevelConf]
	if !ok {
		logLevel = logrus.DebugLevel
	}

	customFormatter := logrus.JSONFormatter{}
	customFormatter.TimestampFormat = timeFormat

	rotateFileHook, err := rotatefilehook.NewRotateFileHook(rotatefilehook.RotateFileConfig{
		Filename:   logFile,
		MaxSize:    maxSize,
		MaxBackups: maxAge,
		MaxAge:     maxAge,
		Level:      logLevel,
		Formatter:  &customFormatter,
	})

	logrus.AddHook(rotateFileHook)
}

// InitLog is used for initialization log environment.
// default logs path is "/data/commlogs/" if "log::logFileRootPath" is not specified.
// automatic create directory if target log path is not exists
func InitLog() error {
	instanceID := os.Getenv("INSTANCE_ID")
	if "" == instanceID {
		logrus.Errorf("Failed to get env:%s", "INSTANCE_ID")
		return errors.New("failed to get env:INSTANCE_ID")
	}

	preLogFileRootPath := beego.AppConfig.DefaultString("log::logFileRootPath", "/data/commlogs/")
	var logFileRootPath string
	if strings.HasSuffix(preLogFileRootPath, "/") {
		logFileRootPath = preLogFileRootPath
	} else {
		logFileRootPath = preLogFileRootPath + "/"
	}
	logPath := logFileRootPath + instanceID
	os.MkdirAll(logPath, os.ModePerm)
	var logFile string
	if beego.AppConfig.String("log::logFile") == "" {
		logFile = logPath + "/comm-agent.log"
	} else {
		logFile = logPath + "/" + beego.AppConfig.String("log::logFile")
	}
	logrus.Debugf("logpathfile==%s", logFile)
	if logFile != "" {
		f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE, 0755)
		if err != nil {
			logrus.Errorf("Failed to open log file %s", logFile)
			return errors.Wrap(err, 0)
		}
		logrus.SetOutput(f)
	}
	logrus.SetOutput(os.Stdout)

	setLogRotateHook(logFile)

	return nil
}

// setLogRotateHook provides the caller with a log rollback policy
func SetLogRotateHook(logFile string, maxSize int, maxAge int, level string) error {
	if logFile != "" {
		f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE, 0755)
		if err != nil {
			logrus.Errorf("Failed to open log file %s", logFile)
			return errors.Wrap(err, 0)
		}
		logrus.SetOutput(f)
	}

	logLevel, ok := logLevelMap[level]
	if !ok {
		logLevel = logrus.DebugLevel
	}

	customFormatter := logrus.JSONFormatter{}
	customFormatter.TimestampFormat = timeFormat
	logrus.SetFormatter(&customFormatter)

	setLogLevel(level)

	rotateFileHook, err := rotatefilehook.NewRotateFileHook(rotatefilehook.RotateFileConfig{
		Filename:   logFile,
		MaxSize:    maxSize,
		MaxBackups: maxAge,
		MaxAge:     maxAge,
		Level:      logLevel,
		Formatter:  &customFormatter,
	})

	logrus.AddHook(rotateFileHook)
	return err
}

// SetLogFields providers caller set all log fields
func SetLogFields(logFieldMap map[string]string) {
	for key, value := range logFieldMap {
		logFields[key] = value
	}
}

// getLogFields providers caller get all log fields
func getLogFields() logrus.Fields {
	return logFields
}

// Infof provides info level with parameter log printing
func Infof(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Infof(format, args...)
	}
}

// Infof provides info level log printing
func Info(args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Info(args...)
	}
}

// Errorf providers error level with parameter log printing
func Errorf(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Errorf(format, args...)
	}
}

// Error providers error level log printing
func Error(args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Error(args...)
	}
}

// Debugf providers debug level with parameter log printing
func Debugf(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Debugf(format, args...)
	}
}

// Debug providers debug level log orinting
func Debug(args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Debug(args...)
	}
}

//---------------------The following code is used to dynamically set the log level----------------------------//

// Server structure is used to the store backend information
type Server struct {
	SocketLocation string
	Debug          bool
}

// StartServerWithDefaults starts the server with default values
func startLogLevelServer(unixSocket string) {
	s := Server{
		SocketLocation: unixSocket,
	}
	s.Start()
}

// Start the server
func (s *Server) Start() {
	os.Remove(s.SocketLocation)
	go s.ListenAndServe()
}

// ListenAndServe is used to setup handlers and
// start listening on the specified location
func (s *Server) ListenAndServe() error {
	logrus.Infof("Listening on %s", s.SocketLocation)
	server := http.Server{}
	http.HandleFunc("/v1/loglevel", s.loglevel)
	socketListener, err := net.Listen("unix", s.SocketLocation)
	if err != nil {
		return errors.Wrap(err, 0)
	}
	return server.Serve(socketListener)
}

// Loglevel provides a way to modify the print level of the log
// by restful mode during system operation.
func (s *Server) loglevel(rw http.ResponseWriter, req *http.Request) {
	// curl -X POST -d "level=debug" localhost:12345/v1/loglevel
	logrus.Debugf("Received loglevel request")
	if req.Method == http.MethodGet {
		level := logrus.GetLevel().String()
		rw.Write([]byte(fmt.Sprintf("%s\n", level)))
	}

	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(fmt.Sprintf("Failed to parse form: %v\n", err)))
		}
		level, err := logrus.ParseLevel(req.Form.Get("level"))
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(fmt.Sprintf("Failed to parse loglevel: %v\n", err)))
		} else {
			logrus.SetLevel(level)
			rw.Write([]byte("OK\n"))
		}
	}
}
