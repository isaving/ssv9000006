package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTR1I struct {
	IntRateNo	string `validate:"required,max=20"`
	EffectDate	string
}

type DAACRTR1O struct {
	IntRateNo     string
	BankNo        string
	Currency      string
	IntRate       float64
	EffectDate    string
	ExpFlag       string
	DayModfyFlag  string
	IntRateName   string
	Flag1         string
	Flag2         string
	Back1         float64
	Back2         float64
	Back3         string
	Back4         string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	IntRateMarket string
	IntRateUnit   string

}

type DAACRTR1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTR1I
}

type DAACRTR1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTR1O
}

type DAACRTR1RequestForm struct {
	Form []DAACRTR1IDataForm
}

type DAACRTR1ResponseForm struct {
	Form []DAACRTR1ODataForm
}

// @Desc Build request message
func (o *DAACRTR1RequestForm) PackRequest(DAACRTR1I DAACRTR1I) (responseBody []byte, err error) {

	requestForm := DAACRTR1RequestForm{
		Form: []DAACRTR1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTR1I",
				},
				FormData: DAACRTR1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTR1RequestForm) UnPackRequest(request []byte) (DAACRTR1I, error) {
	DAACRTR1I := DAACRTR1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTR1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTR1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTR1ResponseForm) PackResponse(DAACRTR1O DAACRTR1O) (responseBody []byte, err error) {
	responseForm := DAACRTR1ResponseForm{
		Form: []DAACRTR1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTR1O",
				},
				FormData: DAACRTR1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTR1ResponseForm) UnPackResponse(request []byte) (DAACRTR1O, error) {

	DAACRTR1O := DAACRTR1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTR1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTR1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTR1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
