package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAILSRC4I struct {
	AttachFileIdList string `json:"attachFileIdList,omitempty"`
	Body             string `json:"body,omitempty"`
	BatchId          string `json:"batchId,omitempty"`
	ObjectKeyType    string `json:"objectKeyType,omitempty"`
	SerialNo		 int
	ObjectKey        string `json:"objectKey,omitempty"`
	FinlModfyDt      string `json:"finlModfyDt,omitempty"`
	FinlModfyTm      string `json:"finlModfyTm,omitempty"`
	FinlModfyOrgNo   string `json:"finlModfyOrgNo,omitempty"`
	FinlModfyTelrNo  string `json:"finlModfyTelrNo,omitempty"`
}

type DAILSRC4O struct {
}

func (o *DAILSRC4I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

func (o *DAILSRC4I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

func (o *DAILSRC4O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

func (o *DAILSRC4O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (i *DAILSRC4I) GetMaps() map[string]interface{} {
	maps := make(map[string]interface{})
	requestBody, err := i.PackRequest()
	if err != nil {
		return nil
	}
	if err := json.Unmarshal(requestBody, &maps); nil != err {
		return nil
	}
	return maps
}

func (o *DAILSRC4I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}
