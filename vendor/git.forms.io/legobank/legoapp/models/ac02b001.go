package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC02B001I struct {
	AcctgAcctNo string `valid:"Required;MaxSize(20)"` //贷款核算账号
	TradeDate   string
}

type AC02B001O struct {
	Records		AC02B001ORecords
}

type AC02B001ORecords []struct {
	AcctgAcctNo		string
	TradeDate		string
	IntacrPrinBal	float64
	PeriodNum		string
	LastMaintDate	string
	LastMaintTime	string
	LastMaintBrno	string
	LastMaintTell	string
}

type AC02B001IDataForm struct {
	FormHead CommonFormHead
	FormData AC02B001I
}

type AC02B001ODataForm struct {
	FormHead CommonFormHead
	FormData AC02B001O
}

type AC02B001RequestForm struct {
	Form []AC02B001IDataForm
}

type AC02B001ResponseForm struct {
	Form []AC02B001ODataForm
}

// @Desc Build request message
func (o *AC02B001RequestForm) PackRequest(AC02B001I AC02B001I) (responseBody []byte, err error) {

	requestForm := AC02B001RequestForm{
		Form: []AC02B001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC02B001I",
				},
				FormData: AC02B001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC02B001RequestForm) UnPackRequest(request []byte) (AC02B001I, error) {
	AC02B001I := AC02B001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC02B001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC02B001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC02B001ResponseForm) PackResponse(AC02B001O AC02B001O) (responseBody []byte, err error) {
	responseForm := AC02B001ResponseForm{
		Form: []AC02B001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC02B001O",
				},
				FormData: AC02B001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC02B001ResponseForm) UnPackResponse(request []byte) (AC02B001O, error) {

	AC02B001O := AC02B001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC02B001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC02B001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC02B001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
