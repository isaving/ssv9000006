package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUAL1I struct {
	AcctgAcctNo           string
	Currency              string
	AcctStatus            string
	CustId                string
	ContId                string
	ProdCode              string
	OrgCreate             string
	AcctCreateDate        string
	MaturityDate          string
	DrawDate              string
	DrawTotBalance        float64
	RepPrinTotTerm        int64
	CurrRepPrinTerm       int64
	NextPrinDate          string
	RepayType             string
	RepayFreq             string
	Balance               float64
	BalanceYesterday      float64
	UnpaidBal             float64
	CavAmt                float64
	LastTranDate          string
	ValidStatus           string
	MgmtOrgId             string
	AcctingOrgId          string
	LastMaintDate         string
	LastMaintTime         string
	LastMaintBrno         string
	LastMaintTell         string
	BalanceTemp1          float64
	BalanceTemp2          float64
	BalanceYesterdayTemp1 float64
	BalanceYesterdayTemp2 float64
}

type DAACUAL1O struct {
	Status	string
}

type DAACUAL1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUAL1I
}

type DAACUAL1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUAL1O
}

type DAACUAL1RequestForm struct {
	Form []DAACUAL1IDataForm
}

type DAACUAL1ResponseForm struct {
	Form []DAACUAL1ODataForm
}

// @Desc Build request message
func (o *DAACUAL1RequestForm) PackRequest(DAACUAL1I DAACUAL1I) (responseBody []byte, err error) {

	requestForm := DAACUAL1RequestForm{
		Form: []DAACUAL1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUAL1I",
				},
				FormData: DAACUAL1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUAL1RequestForm) UnPackRequest(request []byte) (DAACUAL1I, error) {
	DAACUAL1I := DAACUAL1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUAL1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUAL1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUAL1ResponseForm) PackResponse(DAACUAL1O DAACUAL1O) (responseBody []byte, err error) {
	responseForm := DAACUAL1ResponseForm{
		Form: []DAACUAL1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUAL1O",
				},
				FormData: DAACUAL1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUAL1ResponseForm) UnPackResponse(request []byte) (DAACUAL1O, error) {

	DAACUAL1O := DAACUAL1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUAL1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUAL1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUAL1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
