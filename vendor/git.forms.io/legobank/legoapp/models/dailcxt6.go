package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAILCXT6I struct {

}

type DAILCXT6O struct {

}

type DAILCXT6IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAILCXT6ODataForm struct {
	FormHead CommonFormHead
	FormData DAILCXT6O
}

type DAILCXT6RequestForm struct {
	Form []DAILCXT6IDataForm
}

type DAILCXT6ResponseForm struct {
	Form []DAILCXT6ODataForm
}

// @Desc Build request message
func (o *DAILCXT6RequestForm) PackRequest(DAILCXT6I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAILCXT6RequestForm{
		Form: []DAILCXT6IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILCXT6I",
				},
				FormData: DAILCXT6I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAILCXT6RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAILCXT6I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAILCXT6I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILCXT6I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAILCXT6ResponseForm) PackResponse(DAILCXT6O DAILCXT6O) (responseBody []byte, err error) {
	responseForm := DAILCXT6ResponseForm{
		Form: []DAILCXT6ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILCXT6O",
				},
				FormData: DAILCXT6O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAILCXT6ResponseForm) UnPackResponse(request []byte) (DAILCXT6O, error) {

	DAILCXT6O := DAILCXT6O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAILCXT6O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILCXT6O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAILCXT6I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
