package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCBT0I struct {
	AcctgAcctNo    string `validate:"required,max=20"`
	Currency       string
	LoanStatus     string `validate:"required,max=2"`
	AcctType       string
	OrgCreate      string
	AcctCreateDate string
	CurrIntStDate  string
	PeriodNum      int64
	BalanceType    string
	Balance        float64
	CavAmt         float64
	LastTranDate   string
	MgmtOrgId      string
	AcctingOrgId   string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}

type DAACCBT0O struct {
	AcctgAcctNo    string `validate:"required,max=20"`
	Currency       string
	LoanStatus     string `validate:"required,max=2"`
	AcctType       string
	OrgCreate      string
	AcctCreateDate string
	CurrIntStDate  string
	PeriodNum      int64
	BalanceType    string
	Balance        float64
	CavAmt         float64
	LastTranDate   string
	MgmtOrgId      string
	AcctingOrgId   string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}

type DAACCBT0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCBT0I
}

type DAACCBT0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCBT0O
}

type DAACCBT0RequestForm struct {
	Form []DAACCBT0IDataForm
}

type DAACCBT0ResponseForm struct {
	Form []DAACCBT0ODataForm
}

// @Desc Build request message
func (o *DAACCBT0RequestForm) PackRequest(DAACCBT0I DAACCBT0I) (responseBody []byte, err error) {

	requestForm := DAACCBT0RequestForm{
		Form: []DAACCBT0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCBT0I",
				},
				FormData: DAACCBT0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCBT0RequestForm) UnPackRequest(request []byte) (DAACCBT0I, error) {
	DAACCBT0I := DAACCBT0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCBT0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCBT0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCBT0ResponseForm) PackResponse(DAACCBT0O DAACCBT0O) (responseBody []byte, err error) {
	responseForm := DAACCBT0ResponseForm{
		Form: []DAACCBT0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCBT0O",
				},
				FormData: DAACCBT0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCBT0ResponseForm) UnPackResponse(request []byte) (DAACCBT0O, error) {

	DAACCBT0O := DAACCBT0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCBT0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCBT0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCBT0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
