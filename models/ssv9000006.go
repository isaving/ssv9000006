//Version: v1.0.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000006I struct {
	MediaAgreementId	string		//介质和合约关系ID
	MdsType       		string      //介质类型
	MdsNm     			string      //介质号码
	Currency       	 	string      //币别
	CashtranFlag		string		//钞汇标志
	ChannelNm       	string      //渠道号
	UsgCod     			string      //用途代码
	AgreementId			string      //合约号
	AgreementType		string      //合约类型
	DefaultAgreement	string      //默认合约标识
	EffectiveFlag		string      //有效标志
	LastUpDatetime		string      //最后更新日期时间
	LastUpBn     		string      //最后更新行所
	LastUpEm        	string      //最后更新柜员
}

type SSV9000006O struct {
	State string
}

// @Desc Build request message
func (o *SSV9000006I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000006I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000006O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000006O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000006I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000006I) GetServiceKey() string {
	return "sv900006"
}
