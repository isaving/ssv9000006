module git.forms.io/isaving/sv/ssv9000006

go 1.13

require (
	git.forms.io/legobank/legoapp v0.0.0
	git.forms.io/isaving/models v0.0.0
	git.forms.io/universe v0.0.0
	github.com/astaxie/beego v1.12.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.3
	github.com/json-iterator/go v1.1.10
	github.com/pkg/sftp v1.11.0 // indirect
	github.com/urfave/cli v1.22.4
	google.golang.org/api v0.30.0 // indirect
)

replace (
	git.forms.io/legobank/legoapp v0.0.0 => ../../../../git.forms.io/legobank/legoapp
	git.forms.io/isaving/models v0.0.0 => ../../../../git.forms.io/isaving/models
	git.forms.io/universe v0.0.0 => ../../../../git.forms.io/universe

	github.com/tidwall/gjson => github.com/tidwall/gjson v1.3.6
)
