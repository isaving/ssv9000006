package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000006/dao"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/xormplus/xorm"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}


func Test_Success(t *testing.T) {
	input := &models.SSV9000006I{
		MediaAgreementId:"22222",
		MdsType : "1",
		MdsNm : "1",
		Currency:"22222",
		CashtranFlag : "1",
		ChannelNm:"22222",
	}
	impl := Ssv9000006Impl{
		Ssv9000006I: input,
		//O:         dao.EngineCache,
	}
	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db

			//mock.ExpectQuery("t_mdslagp").WillReturnRows(sqlmock.NewRows([]string{"media_agreement_id"}).AddRow("1000001"))
			mock.ExpectExec("t_mdslagp").WillReturnResult(sqlmock.NewResult(0,1))
			mock.ExpectExec("t_mdslagp").WillReturnResult(sqlmock.NewResult(0,1))
			mock.ExpectExec("t_mdslagp").WillReturnResult(sqlmock.NewResult(0,1))

			Convey("Validate session", func() {
				_, _ = impl.TrySsv9000006(input)
				_, _ = impl.ConfirmSsv9000006(input)
				_, _ = impl.CancelSsv9000006(input)
			})
		})

	})
}

func Test_Error(t *testing.T) {
	input := &models.SSV9000006I{
		MediaAgreementId:"22222",
		MdsType : "1",
		MdsNm : "1",
		Currency:"22222",
		CashtranFlag : "1",
		ChannelNm:"22222",
	}
	impl := Ssv9000006Impl{
		Ssv9000006I: input,
		//O:         dao.EngineCache,
	}
	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db

			//mock.ExpectQuery("t_mdslagp").WillReturnRows(sqlmock.NewRows([]string{"media_agreement_id"}))
			mock.ExpectExec("t_mdslagp").WillReturnError(errors.New("1"))
			Convey("Validate session", func() {
				_, _ = impl.TrySsv9000006(input)
				_, _ = impl.ConfirmSsv9000006(input)
				_, _ = impl.CancelSsv9000006(input)
			})
		})

	})
}

func Test_insert(t *testing.T) {
		addr := "localhost:3306"
		userName := "root"
		password := "123456"
		dbName := "legobank_sv"
		dsn := userName + ":" + password + "@tcp(" + addr + ")/" + dbName
		engine, err := xorm.NewEngine("mysql", dsn)
		if err != nil {
			return
		}
		maxIdleConns := beego.AppConfig.DefaultInt("mysql::maxIdleConns", 30)
		maxOpenConns := beego.AppConfig.DefaultInt("mysql::maxOpenConns", 30)
		maxLifeValue := beego.AppConfig.DefaultInt("mysql::maxLifeValue", 540)
		showSql := beego.AppConfig.DefaultBool("mysql::showSql", true)

		engine.SetMaxIdleConns(maxIdleConns)
		engine.SetMaxOpenConns(maxOpenConns)
		engine.SetConnMaxLifetime(time.Duration(maxLifeValue)*time.Second)
		engine.ShowSQL(showSql)

		EngineCache := engine
	insert := &dao.TMdslagp{
		MediaAgreementId:"222122",
		MdsType : "1",
		MdsNm : "1",
		Currency:"2",
		CashtranFlag : "1",
		ChannelNm:"2",
		LastUpDatetime:"2020-01-02",
	}
	n, err := EngineCache.Insert(insert)
	fmt.Println(n,err)
	sql := "update t_mdslagp set tcc_state=0 where media_agreement_id=?"
	if _, err := EngineCache.Exec(sql, "1"); nil != err {
		fmt.Println(n,err)
	}
}
