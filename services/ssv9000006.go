//Version: v1.0.0.0
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000006/dao"
	"git.forms.io/legobank/legoapp/services"
    dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)
var Ssv9000006Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv9000006",
	ConfirmMethod: "ConfirmSsv9000006",
	CancelMethod:  "CancelSsv9000006",
}

type Ssv9000006 interface {
	TrySsv9000006(*models.SSV9000006I) (*models.SSV9000006O, error)
	ConfirmSsv9000006(*models.SSV9000006I) (*models.SSV9000006O, error)
	CancelSsv9000006(*models.SSV9000006I) (*models.SSV9000006O, error)
}

type Ssv9000006Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
    Ssv9000006O         *models.SSV9000006O
    Ssv9000006I         *models.SSV9000006I
}
// @Desc Ssv9000006 process
// @Author
// @Date 2020-12-20
func (impl *Ssv9000006Impl) TrySsv9000006(ssv9000006I *models.SSV9000006I) (ssv9000006O *models.SSV9000006O, err error) {

	TMdslagp := &dao.TMdslagp{
		MediaAgreementId: ssv9000006I.MediaAgreementId,
		MdsType:          ssv9000006I.MdsType        ,
		MdsNm:            ssv9000006I.MdsNm          ,
		Currency:         ssv9000006I.Currency       ,
		CashtranFlag:     ssv9000006I.CashtranFlag   ,
		ChannelNm:        ssv9000006I.ChannelNm      ,
		UsgCod:           ssv9000006I.UsgCod         ,
		AgreementId:      ssv9000006I.AgreementId    ,
		AgreementType:    ssv9000006I.AgreementType  ,
		DefaultAgreement: ssv9000006I.DefaultAgreement,
		EffectiveFlag:    ssv9000006I.EffectiveFlag  ,
		LastUpDatetime:   ssv9000006I.LastUpDatetime ,
		LastUpBn:         ssv9000006I.LastUpBn       ,
		LastUpEm:         ssv9000006I.LastUpEm       ,
	}
	if err := dao.InsertTMdslagp(TMdslagp,dao.EngineCache); nil != err {
		return nil, err
	}

	ssv9000006O = &models.SSV9000006O{
		State:"Ok",
	}

	return ssv9000006O, nil
}

func (impl *Ssv9000006Impl) ConfirmSsv9000006(ssv9000006I *models.SSV9000006I) (ssv9000006O *models.SSV9000006O, err error) {
	sql := "update t_mdslagp set tcc_state=0 where media_agreement_id=?"
	if _, err := dao.EngineCache.Exec(sql, ssv9000006I.MediaAgreementId); nil != err {
		return nil, err
	}

	log.Debug("Start confirm ssv9000006")
	return nil, nil
}

func (impl *Ssv9000006Impl) CancelSsv9000006(ssv9000006I *models.SSV9000006I) (ssv9000006O *models.SSV9000006O, err error) {
	sql := "delete from t_mdslagp where media_agreement_id=? and tcc_state=1"
	if _, err = dao.EngineCache.Exec(sql,ssv9000006I.MediaAgreementId); nil != err {
		return nil, err
	}
	log.Debug("Start cancel ssv9000006")
	return nil, nil
}
