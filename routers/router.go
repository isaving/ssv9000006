////Version: v1.0.0.0
package routers

import (
	"git.forms.io/isaving/sv/ssv9000006/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-20
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	eventRouteReg.Router("SV900006",
		&controllers.Ssv9000006Controller{}, "Ssv9000006")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-20
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv9000006",
		beego.NSNamespace("/ssv9000006",
			beego.NSInclude(
				&controllers.Ssv9000006Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
